package com.test.prices;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.prices.domain.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.JavaTimeConversionPattern;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.repository.query.Param;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SpringBootTest
@AutoConfigureMockMvc
class PricesApplicationTests {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @ParameterizedTest
  @CsvFileSource(resources = "/prices.csv" , delimiter = ';')
  void testPrice1(int brandId, int productId, @JavaTimeConversionPattern("yyyy/MM/dd HH:mm:ss") LocalDateTime dateTime, BigDecimal finalPrice) throws Exception {
    MvcResult result = mockMvc.perform(
            MockMvcRequestBuilders.get("/price/{brandId}/{productId}/{date}", brandId, productId, dateTime))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
    Price price = objectMapper.readValue(result.getResponse().getContentAsString(), Price.class);
    Assertions.assertTrue(price.getPrice().equals(finalPrice));

  }
}
