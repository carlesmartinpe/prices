package com.test.prices.application;

import com.test.prices.domain.port.in.PriceRetrieverPort;
import com.test.prices.domain.Price;
import com.test.prices.domain.port.out.PriceLoadPersistencePort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class PriceServiceImpl implements PriceRetrieverPort {

  private final  PriceLoadPersistencePort priceLoadPersistencePort;
  
  public PriceServiceImpl(PriceLoadPersistencePort priceLoadPersistencePort){
    this.priceLoadPersistencePort =  priceLoadPersistencePort;
  }
  @Override
  public Price getPrice(int brandId, int productId, LocalDateTime date) {
    return priceLoadPersistencePort.getEffectivePrice(brandId,productId,date);
  }

}
