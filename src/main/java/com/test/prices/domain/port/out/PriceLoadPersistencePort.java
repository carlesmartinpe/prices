package com.test.prices.domain.port.out;

import com.test.prices.domain.Price;

import java.time.LocalDateTime;


public interface PriceLoadPersistencePort {
  Price getEffectivePrice(int brandId, int productId, LocalDateTime date);
}
