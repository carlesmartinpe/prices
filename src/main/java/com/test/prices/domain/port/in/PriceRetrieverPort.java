package com.test.prices.domain.port.in;

import com.test.prices.domain.Price;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

public interface PriceRetrieverPort {

  Price getPrice(int brandId, int productId, LocalDateTime date);
}
