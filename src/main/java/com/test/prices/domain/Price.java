package com.test.prices.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Setter
@Getter
public class Price {
  
  private int productId;
  private int brandId;
  private LocalDateTime startDate;
  private LocalDateTime endDate;
  private String curr;
  private BigDecimal price;
}
