package com.test.prices.infraestructure.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PriceRepository extends CrudRepository<PriceEntity,Long> {
  
  @Query("select p from PriceEntity p where p.brandId = :brandId and p.productId = :productId and  :date between p.startDate  and p.endDate order by p.priority desc")
  List<PriceEntity> getEffectivePrice(@Param("brandId") int brandId, @Param("productId") int productId,@Param("date") LocalDateTime date);
  
}
