package com.test.prices.infraestructure.persistence.adapter;

import com.test.prices.domain.Price;
import com.test.prices.infraestructure.persistence.PriceEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PriceMapper {
  
  Price mapPriceEntityToPrice(PriceEntity src);
  
}
