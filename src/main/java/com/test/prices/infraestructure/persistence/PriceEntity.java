package com.test.prices.infraestructure.persistence;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Table(name="PRICES")
@Entity
@Getter
@Setter
public class PriceEntity {
  @Id
  private Long id;

  @Column(name = "BRAND_ID")
  private int brandId;
  @Column(name="PRODUCT_ID")
  private int productId;
  @Column(name="START_DATE")
  private LocalDateTime startDate;
  @Column(name="END_DATE")
  private LocalDateTime endDate;
  private String curr;
  private BigDecimal price;
  private int priority;
  @Column(name="PRICE_LIST")
  private int priceList;
  

  
  
}
