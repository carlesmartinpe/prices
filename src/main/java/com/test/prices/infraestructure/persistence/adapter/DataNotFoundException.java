package com.test.prices.infraestructure.persistence.adapter;

public class DataNotFoundException extends RuntimeException{
  
  public DataNotFoundException(String message){
    super(message);
  }
}
