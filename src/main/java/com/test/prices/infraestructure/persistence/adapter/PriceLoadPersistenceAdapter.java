package com.test.prices.infraestructure.persistence.adapter;

import com.test.prices.domain.Price;
import com.test.prices.infraestructure.persistence.PriceEntity;
import com.test.prices.infraestructure.persistence.PriceRepository;
import com.test.prices.domain.port.out.PriceLoadPersistencePort;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;

@Component
public class PriceLoadPersistenceAdapter implements PriceLoadPersistencePort {

  private PriceRepository priceRepository;
  private PriceMapper priceMapper;
  
  public PriceLoadPersistenceAdapter(PriceRepository priceRepository, PriceMapper priceMapper){
    this.priceRepository = priceRepository;
    this.priceMapper = priceMapper;
  }
  
  @Override
  public Price getEffectivePrice(int brandId, int productId, LocalDateTime date) {
    PriceEntity priceEntity =  priceRepository.getEffectivePrice(brandId,productId,date).stream().findFirst().orElseThrow(()-> new DataNotFoundException("Price not found"));
    return priceMapper.mapPriceEntityToPrice(priceEntity);
  }
}
