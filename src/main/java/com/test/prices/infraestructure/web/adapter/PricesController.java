package com.test.prices.infraestructure.web.adapter;

import com.test.prices.domain.port.in.PriceRetrieverPort;
import com.test.prices.domain.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class PricesController {
  
  @Autowired
  private PriceRetrieverPort priceRetrieverPort;
  
  @GetMapping("/price/{brandId}/{productId}/{date}")
  public ResponseEntity<Price> getPriceForProduct(@PathVariable int brandId, @PathVariable int productId, @PathVariable LocalDateTime date){
           return new ResponseEntity(priceRetrieverPort.getPrice(brandId,productId,date), HttpStatus.OK);
  }
}
