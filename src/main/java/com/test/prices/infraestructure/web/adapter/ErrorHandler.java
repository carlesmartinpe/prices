package com.test.prices.infraestructure.web.adapter;

import com.sun.net.httpserver.HttpsServer;
import com.test.prices.infraestructure.persistence.adapter.DataNotFoundException;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ErrorHandler {
  
  
  @ExceptionHandler({DataNotFoundException.class})
  public ResponseEntity<String> handleNotFound(DataNotFoundException e, WebRequest request){
    return new ResponseEntity<>("No price found "+e.getMessage(), HttpStatus.NOT_ACCEPTABLE );
  }
  
  @ExceptionHandler({Exception.class})
  public ResponseEntity<String> handdleError(Exception e, WebRequest request){
    return new ResponseEntity<>("Internal error: "+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR );
  }
  
  
}
